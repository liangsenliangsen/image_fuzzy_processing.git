//
//  main.m
//  图片的模糊处理
//
//  Created by 梁森 on 2018/3/20.
//  Copyright © 2018年 梁森. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
