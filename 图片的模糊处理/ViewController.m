//
//  ViewController.m
//  图片的模糊处理
//
//  Created by 梁森 on 2018/3/20.
//  Copyright © 2018年 梁森. All rights reserved.
//

#import "ViewController.h"
#import "UIImageView+blurry.h"
#import "UIImage+ImageEffects.h"

#import <GPUImage/GPUImage.h>
@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageV;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor greenColor];
    self.imageV.image = [UIImage imageNamed:@"timg.jpg"];
    self.imageV.contentMode = UIViewContentModeScaleAspectFill;
    self.imageV.clipsToBounds = YES;
    
    
//    self.imageV.image = [UIImageView blurryImage:self.imageV.image withBlurLevel:0.5];
//    self.imageV.image = [UIImageView blurryCoreImage:self.imageV.image withBlurLevel:1];
    
       
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    //    45
    
    // UIImage+ImageEffects
    self.imageV.image = [self.imageV.image blurImageWithRadius:10];
    
    //  vImage
//    [self.imageV setImageToBlur:self.imageV.image blurRadius:30 completionBlock:^{
//        
//    }];
    // 高斯模糊
//    self.imageV.image = [self filterWith:self.imageV.image andRadius:10];
    //
//    [self GPUImage];
    
    
}
#pragma mark --- GPUImage处理图片模糊
- (void)GPUImage{
    GPUImageGaussianBlurFilter *gaussianBlurFilter = [[GPUImageGaussianBlurFilter alloc] init];
    [gaussianBlurFilter forceProcessingAtSize:CGSizeMake(self.imageV.frame.size.width, self.imageV.frame.size.height)];
    
    //    GPUImageGaussianBlurFilter *gaussianBlurFilter = [[GPUImageGaussianBlurFilter alloc] init];
    gaussianBlurFilter.blurRadiusInPixels = 1;
    UIImage *blurredImage = [gaussianBlurFilter imageByFilteringImage: self.imageV.image];
    self.imageV.image = blurredImage;
    
    [gaussianBlurFilter useNextFrameForImageCapture];
    //获取数据源
    GPUImagePicture *stillImageSource = [[GPUImagePicture alloc]initWithImage:self.imageV.image];
    //添加上滤镜
    [stillImageSource addTarget:gaussianBlurFilter];
    //开始渲染
    [stillImageSource processImage];
    //获取渲染后的图片
    UIImage *newImage = [gaussianBlurFilter imageFromCurrentFramebuffer];
    self.imageV.image = newImage;
    
}
#pragma mark --- CIFilter进行模糊处理（高斯模糊）
- (UIImage *)filterWith:(UIImage *)image andRadius:(CGFloat)radius{
    
    CIImage *inputImage = [[CIImage alloc] initWithCGImage:image.CGImage];
    
    CIFilter *affineClampFilter = [CIFilter filterWithName:@"CIAffineClamp"];
    CGAffineTransform xform = CGAffineTransformMakeScale(1.0, 1.0);
    [affineClampFilter setValue:inputImage forKey:kCIInputImageKey];
    [affineClampFilter setValue:[NSValue valueWithBytes:&xform
                                               objCType:@encode(CGAffineTransform)]
                         forKey:@"inputTransform"];
    
    CIImage *extendedImage = [affineClampFilter valueForKey:kCIOutputImageKey];
    
    CIFilter *blurFilter =
    [CIFilter filterWithName:@"CIGaussianBlur"];
    [blurFilter setValue:extendedImage forKey:kCIInputImageKey];
    [blurFilter setValue:@(radius) forKey:@"inputRadius"];
    
    CIImage *result = [blurFilter valueForKey:kCIOutputImageKey];
    
    CIContext *ciContext = [CIContext contextWithOptions:nil];
    
    CGImageRef cgImage = [ciContext createCGImage:result fromRect:inputImage.extent];
    
    UIImage *uiImage = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    
    return uiImage;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
